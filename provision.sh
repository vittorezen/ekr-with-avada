#!/usr/bin/env bash


echo "Update packages"
cp -r /vagrant/provision/linuxConfiguration/etc/apt/* /etc/apt
apt-get update -q

echo "Installing packages - extra"
apt-get install sudo wget curl git-core  vim elinks -y

echo "Installing packages - apache"
apt-get install apache2  -y
echo "Installing packages - php"
apt-get install php5-common libapache2-mod-php5 php5-dev php5-cli php5-curl \
        php5-gd php5-mcrypt php5-mysql php5-dev php5-dev  php5-xdebug -y




if [ ! -f  /usr/local/bin/composer ]; then
    echo "Get composer"
    curl -sS https://getcomposer.org/installer | php  
    mv composer.phar /usr/local/bin/composer  
    chmod a+x /usr/local/bin/composer 
fi
if ! [ -L /var/www ]; then
    echo "Check apache2"
    cp -r /vagrant/provision/linuxConfiguration/etc/apache2/* /etc/apache2
    cp -r /vagrant/provision/linuxConfiguration/etc/php5/* /etc/php5
    a2enmod rewrite 
    service apache2 restart
fi
if [ ! -f /usr/sbin/mysqld ]; then
    echo "Installing packages - mysql"
    DEBIAN_FRONTEND=noninteractive apt-get  -y install mysql-server
    mysqladmin -u root password root
fi